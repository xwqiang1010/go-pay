module gitee.com/xwqiang1010/go-pay

go 1.12

require (
	github.com/smartwalle/alipay/v3 v3.1.8
	github.com/smartwalle/crypto4go v1.0.3
)
